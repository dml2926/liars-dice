﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDice : Player 
{
    public GameObject Name;

    void getNewPosi(int i)
    {
        if (i == 0)
        {
            dieArray[i].newPosition = new Vector3(2.5f, 0.5f, -3.81f);
        }

    }

    void Start()
    {
        for (int i = 0; i < 6; i++)
        {
            if (Name == dieArray[i].Dice)
            {
                dieArray[i].rb = GetComponent<Rigidbody>();
            }
        }
    }

    void Update()
    {
        for (int i = 0; i < 6; i++){
            if (Name == dieArray[i].Dice)
            {
                dieArray[i].diceVelocity = dieArray[i].rb.velocity;

                getNewPosi(i);

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    DiceText.diceNum = 0;
                    float dirX = Random.Range(0, 500);
                    float dirY = Random.Range(0, 500);
                    float dirZ = Random.Range(0, 500);
                    transform.position = new Vector3(0, 1, -9);
                    transform.rotation = Quaternion.identity;
                    dieArray[i].rb.AddForce(transform.up * 500);
                    dieArray[i].rb.AddTorque(dirX, dirY, dirZ);
                }
                transform.position = Vector3.Lerp(transform.position, dieArray[i].newPosition, Time.deltaTime * 2);
            }
        }
    }
}
