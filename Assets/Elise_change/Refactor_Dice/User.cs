﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User : MonoBehaviour
{
    // Global variables
    public int numOfDie = 6;                    // original dice number
    public string userName = "Taylor Swift";    //placeholder userName


    //*****************************************************
    // nameGenerator func (string)
    //  Assigns Dice name to be compared to in UserDie.cs
    //*****************************************************
    string nameGenerator(int diceNum)
    {
        string stringNum = diceNum.ToString();
        string thisDice = "PD" + stringNum;

        return thisDice;
    }

    //*****************************************************
    // randomFace func (int)
    //  Assigns the randomized dice face value (1-6).
    //*****************************************************
    int randomFace()
    {
        int diceRand;
        return diceRand = Random.Range(1, 6);
    }

    //*****************************************************
    // CurrDie Class
    //  Class constructor to hold values for the current die. 
    //      CurrDieName = string for the name of the current die
    //      ifEnabled = bool for is object is enabled in play
    //      randVal = int for randmoized dice value to be used
    //                  for what side faces up.
    //*****************************************************
    public class CurrDie
    {
        public string CurrDieName;
        public bool isEnabled;
        public int randVal;

        public CurrDie(string diceName, bool enabledVal, int randomVal)
        {
            CurrDieName = diceName;
            isEnabled = enabledVal;
            randVal = randomVal;
        }
    }

    //*****************************************************
    // UserInfo Class
    //  Class constructor to hold username and a list of CurrDie
    //  objects.
    //      playerName = string of user's name
    //      PlayerList = List of CurrDie objects
    //*****************************************************
    public class UserInfo
    {
        private string playerName;
        public List<CurrDie> PlayerList = new List<CurrDie>();

        public UserInfo(string userName, List<CurrDie> listIn)
        {
            playerName = userName;
            PlayerList = listIn;
        }
    }

    UserInfo PlayerInfo;        // global UserInfo object to be called in Start() and Update()

    //*****************************************************
    // GetList (List<CurrDie>)
    //  Returns the UserInfo.PlayerList to be called in UserDie.cs
    //*****************************************************
    public List<CurrDie> GetList()
    {
        return PlayerInfo.PlayerList;
    }

    //*****************************************************
    // Start func (void)
    //  Prepacked function in Unity. Takes in the username 
    //  and creates a list of CurrDie objects to be passed 
    //  in the UserInfo class.
    //*****************************************************
    void Start()
    {
        List<CurrDie> holder = new List<CurrDie>();     //local list

        for (int i = 1; i < (numOfDie+1); i++)
        {
            string dieName = nameGenerator(i);
            int dieVal = randomFace();

            CurrDie newDie = new CurrDie(dieName, true, dieVal);
            holder.Add(newDie); 
        }

        PlayerInfo = new UserInfo(userName, holder);
    }

    //*****************************************************
    // Update func (void)
    //  Prepacked function in Unity, called once per frame.
    //  Uses Keystoke inputs to change the face values or
    //  remove the dice.
    //      -> Keystroke 'Space' = changes face values
    //                   'Backspace' = removed dice starting
    //                                 from dice 6.
    //*****************************************************
    void Update()
    {
        //only change dice val on space
        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (var el in PlayerInfo.PlayerList)
            {
                el.randVal = randomFace();
            }
        }

        //Remove dice oject (isEnabled) in backspace
        else if (Input.GetKeyDown(KeyCode.Backspace))
        {
            PlayerInfo.PlayerList[numOfDie-1].isEnabled = false;
            numOfDie--;
        }
    }
}
