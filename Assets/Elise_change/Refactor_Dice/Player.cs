﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static int enabledDice;

    public class PlayerDie
    {
        public GameObject Dice;
        public Rigidbody rb;
        public Vector3 diceVelocity;
        public Vector3 newPosition;
    }

    public PlayerDie[] dieArray = new PlayerDie[6];

    GameObject assignName(int num)
    {
        string tempName = "dice" + num;
        GameObject realName = GameObject.Find(tempName);
        return realName;
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 6; i++)
        {
            dieArray[i].Dice = assignName(i + 1);
            Debug.Log(dieArray[i].Dice.ToString()); 
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
