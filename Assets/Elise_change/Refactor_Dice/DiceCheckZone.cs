﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZone : Player
{
    Vector3 diceVelocity;

    void FixedUpdate()
    {
        for (int i = 0; i < 6; i++)
        {
            diceVelocity = dieArray[i].diceVelocity;
        }
    }

    void OnTriggerStay(Collider col)
    {
        if (diceVelocity.x == 0f && diceVelocity.y == 0f && diceVelocity.z == 0f)
        {
            switch (col.gameObject.name)
            {
                case "Side1":
                    DiceText.diceNum = 6;
                    break;
                case "Side2":
                    DiceText.diceNum = 5;
                    break;
                case "Side3":
                    DiceText.diceNum = 4;
                    break;
                case "Side4":
                    DiceText.diceNum = 3;
                    break;
                case "Side5":
                    DiceText.diceNum = 2;
                    break;
                case "Side6":
                    DiceText.diceNum = 1;
                    break;
            }
        }

    }
}
