﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserDie : MonoBehaviour
{
    public string Name;         //Name given as a type in string in Unity GUI
    private User userManager;

    //*****************************************************
    // ThisDie Class
    //  Class object to hold CurrDie values from 
    //  PlayerInfo.PlayerList
    //      ifEnabled = bool for is object is enabled in play
    //      faceVal = int for randmoized dice value to be used
    //                  for what side faces up.
    //*****************************************************
    private class ThisDie
    {
        public bool ifEnabled;
        public int faceVal;

        public ThisDie(bool avail, int imageVal)
        {
            ifEnabled = avail;
            faceVal = imageVal;
        }
    }

    //*****************************************************
    // faceRotate func (void)
    //  Takes in the face value from ThisDie.faceVal and 
    //  roatets dice object to correct face up position.
    //*****************************************************
    void faceRotate(int face)
    {
        switch (face)
        {
            case 1:
                transform.Rotate(180, 0, 0);
                break;
            case 2:
                transform.Rotate(0, 0, 90);
                break;
            case 3:
                transform.Rotate(90, 0, 0);
                break;
            case 4:
                transform.Rotate(270, 0, 0);
                break;
            case 5:
                transform.Rotate(0, 0, 270);
                break;
            case 6:
                transform.Rotate(0, 0, 0);
                break;
        }
    }

    // global variables called for Update func
    private ThisDie create;     // ThisDie object to put values in ThisDie class

    public bool placeAvail;     // bool placeholder for the value taken from PlayerInfo.PlayerList
    public int placeImgVal;     // int placeholder for ...  PlayerInfo.PlayerList

    void Start()
    {
        // Empty Start func
    }

    //*****************************************************
    // Update func (void)
    //  Prepacked in Unity, called once every frame. Takes
    //  in the PlayerInfo.PlayerList from User.cs and puts
    //  the values of isEnabled and randVal into ThisDie class.
    //  Based on Keystroke input to perform object actions in
    //  play mode. Includes Debug.Log to display values taken
    //  to compare to face up on the dice object.
    //    ->Keystroke 'Space' = new dice values, face up.
    //                'Backspace' = remove dice starting from PD6.
    //*****************************************************
    void Update()
    {
        userManager = GameObject.FindObjectOfType<User>();
        List<User.CurrDie> placeHolder = userManager.GetList();

        for (int i = 0; i < placeHolder.Count; i++)
        {
            if (placeHolder[i].CurrDieName == Name)
            {
                placeAvail = placeHolder[i].isEnabled;
                placeImgVal = placeHolder[i].randVal;

                create = new ThisDie(placeAvail, placeImgVal);
            }
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            gameObject.SetActive(create.ifEnabled);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.rotation = Quaternion.identity;
            Debug.Log(Name);
            Debug.Log(create.faceVal);
            faceRotate(create.faceVal);
        }
    }
}
