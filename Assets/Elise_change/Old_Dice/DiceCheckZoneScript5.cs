using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZoneScript5 : MonoBehaviour {
    Vector3 diceVelocity5;

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            GetComponent<Collider>().enabled = false;
            diceVelocity5.x = -1;
            diceVelocity5.y = -1;
            diceVelocity5.z = -1;
            return;
        }
        diceVelocity5 = DiceScript5.diceVelocity;
    }

    private void OnTriggerEnter(Collider col5)
    {
        Debug.Log("Hehe");
    }

    void OnTriggerStay(Collider col5)
	{
        if (diceVelocity5.x == 0f && diceVelocity5.y == 0f && diceVelocity5.z == 0f)
        {
            switch (col5.gameObject.name)
            {
                case "Side1":
                    DiceNumberTextScript5.diceNumber5 = 6;
                    break;
                case "Side2":
                    DiceNumberTextScript5.diceNumber5 = 5;
                    break;
                case "Side3":
                    DiceNumberTextScript5.diceNumber5 = 4;
                    break;
                case "Side4":
                    DiceNumberTextScript5.diceNumber5 = 3;
                    break;
                case "Side5":
                    DiceNumberTextScript5.diceNumber5 = 2;
                    break;
                case "Side6":
                    DiceNumberTextScript5.diceNumber5 = 1;
                    break;
            }
        }
    }

    void OnTriggerExit(Collider col5)
    {
        if (GetComponent<Collider>().enabled == false)
        {
            col5.enabled = false;
        }
    }
}
