using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceNumberTextScript2 : MonoBehaviour {

	Text text;
	//public static int diceNumber;
    public static int diceNumber2;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            GetComponent<Text>().enabled = false;
        }
        text.text = diceNumber2.ToString ();
	}
}
