using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScript6 : MonoBehaviour {

	static Rigidbody rb6;
	public static Vector3 diceVelocity;
    public static Vector3 diceVelocity6;
    static Vector3 newPosition;

    // Use this for initialization
    void Start () {
		rb6 = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		diceVelocity6 = rb6.velocity;
        newPosition = new Vector3(-2.5f, 0.5f, -3.81f);

        if (Input.GetKeyDown (KeyCode.Space)) {
			DiceNumberTextScript1.diceNumber = 0;
			float dirX = Random.Range (0, 500);
			float dirY = Random.Range (0, 500);
			float dirZ = Random.Range (0, 500);
			transform.position = new Vector3 (0, 1, -9);
			transform.rotation = Quaternion.identity;
			rb6.AddForce (transform.up * 500);
			rb6.AddTorque (dirX, dirY, dirZ);
		}
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            gameObject.SetActive(!gameObject.activeSelf);
            rb6.detectCollisions = false;
            DiceNumberTextScript6.diceNumber6 = 0;
        }
        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * 2);
    }
}
