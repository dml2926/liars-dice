using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZoneScript2 : MonoBehaviour {
    Vector3 diceVelocity2;

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            GetComponent<Collider>().enabled = false;
            return;
        }
        diceVelocity2 = DiceScript2.diceVelocity;
    }

    /*private void OnTriggerEnter(Collider col2)
    {
        Debug.Log("Hehe");
        if (GetComponent<Collider>().enabled == false)
        {
            col2.enabled = false;
        }
    }*/

    void OnTriggerStay(Collider col2)
	{
        if (GetComponent<Collider>().enabled == false)
        {
            col2.enabled = false;
            //Debug.Log("collider is inactive, skip collision handling");
            //return;
        }
        else if (diceVelocity2.x == 0f && diceVelocity2.y == 0f && diceVelocity2.z == 0f)
        {
            switch (col2.gameObject.name)
            {
                case "Side1":
                    DiceNumberTextScript2.diceNumber2 = 6;
                    break;
                case "Side2":
                    DiceNumberTextScript2.diceNumber2 = 5;
                    break;
                case "Side3":
                    DiceNumberTextScript2.diceNumber2 = 4;
                    break;
                case "Side4":
                    DiceNumberTextScript2.diceNumber2 = 3;
                    break;
                case "Side5":
                    DiceNumberTextScript2.diceNumber2 = 2;
                    break;
                case "Side6":
                    DiceNumberTextScript2.diceNumber2 = 1;
                    break;
            }
        }
    }

    void OnTriggerExit(Collider col2)
    {
        if (GetComponent<Collider>().enabled == false)
        {
            col2.enabled = false;
        }
    }
}
