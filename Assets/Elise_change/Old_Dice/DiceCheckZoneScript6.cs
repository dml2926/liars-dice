using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZoneScript6 : MonoBehaviour {
    Vector3 diceVelocity6;

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            GetComponent<Collider>().enabled = false;
            return;
        }
        diceVelocity6 = DiceScript6.diceVelocity;
    }

    void OnTriggerEnter(Collider col6)
    {
        Debug.Log("Nothing to see here.");
        if (GetComponent<Collider>().enabled == false)
        {
            col6.enabled = false;
            //Debug.Log("collider is inactive, skip collision handling");
            //return;
        }
    }

    void OnTriggerStay(Collider col6)
	{
        if (diceVelocity6.x == 0f && diceVelocity6.y == 0f && diceVelocity6.z == 0f)
        {
            switch (col6.gameObject.name)
            {
                case "Side1":
                    DiceNumberTextScript6.diceNumber6 = 6;
                    break;
                case "Side2":
                    DiceNumberTextScript6.diceNumber6 = 5;
                    break;
                case "Side3":
                    DiceNumberTextScript6.diceNumber6 = 4;
                    break;
                case "Side4":
                    DiceNumberTextScript6.diceNumber6 = 3;
                    break;
                case "Side5":
                    DiceNumberTextScript6.diceNumber6 = 2;
                    break;
                case "Side6":
                    DiceNumberTextScript6.diceNumber6 = 1;
                    break;
            }
        }
    }

    void OnTriggerExit(Collider col6)
    {
        if (GetComponent<Collider>().enabled == false)
        {
            col6.enabled = false;
            //Debug.Log("collider is inactive, skip collision handling");
            //return;
        }
    }
}
