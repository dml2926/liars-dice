using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScript3 : MonoBehaviour {

	static Rigidbody rb3;
	public static Vector3 diceVelocity;
    public static Vector3 diceVelocity3;
    static Vector3 newPosition;

    // Use this for initialization
    void Start () {
		rb3 = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		diceVelocity3 = rb3.velocity;
        newPosition = new Vector3(0.5f, 0.5f, -3.81f);

        if (Input.GetKeyDown (KeyCode.Space)) {
			DiceNumberTextScript1.diceNumber = 0;
			float dirX = Random.Range (0, 500);
			float dirY = Random.Range (0, 500);
			float dirZ = Random.Range (0, 500);
			transform.position = new Vector3 (0, 1, -9);
			transform.rotation = Quaternion.identity;
			rb3.AddForce (transform.up * 500);
			rb3.AddTorque (dirX, dirY, dirZ);
		}
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            gameObject.SetActive(!gameObject.activeSelf);
            rb3.detectCollisions = false;
            DiceNumberTextScript3.diceNumber3 = 0;
        }
        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * 2);
    }
}
