using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScript : MonoBehaviour {

    //public GameObject Name;

	public static Rigidbody rb;
	public static Vector3 diceVelocity;
    public static Vector3 newPosition; 

    public virtual Rigidbody getRb()
    {
        rb = GetComponent<Rigidbody>();
        return rb;
    }

    public virtual void newPosi()
    {
        newPosition = new Vector3(2.5f, 0.5f, -3.81f); 
    }

	// Use this for initialization
	void Start () {
        //rb = GetComponent<Rigidbody> ();
        getRb();
	}
	
	// Update is called once per frame
	void Update () {
        diceVelocity = getRb().velocity;
        //diceVelocity = rb.velocity;
        newPosi();

        if (Input.GetKeyDown (KeyCode.Space)) {
			DiceNumberTextScript1.diceNumber1 = 0;
			float dirX = Random.Range (0, 500);
			float dirY = Random.Range (0, 500);
			float dirZ = Random.Range (0, 500);
			transform.position = new Vector3 (0, 1, -9);
			transform.rotation = Quaternion.identity;
			rb.AddForce (transform.up * 500);
			rb.AddTorque (dirX, dirY, dirZ);
		}
        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * 2);
    }
}
