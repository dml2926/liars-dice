using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZoneScript3 : MonoBehaviour {
    Vector3 diceVelocity3;

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            GetComponent<Collider>().enabled = false;
            diceVelocity3.x = -1;
            diceVelocity3.y = -1;
            diceVelocity3.z = -1;
            return;
        }
        diceVelocity3 = DiceScript3.diceVelocity;
    }

	void OnTriggerStay(Collider col3)
	{
        if (GetComponent<Collider>().enabled == false)
        {
            col3.enabled = false;
            //Debug.Log("collider is inactive, skip collision handling");
            //return;
        }
        else if (diceVelocity3.x == 0f && diceVelocity3.y == 0f && diceVelocity3.z == 0f)
        {
            switch (col3.gameObject.name)
            {
                case "Side1":
                    DiceNumberTextScript3.diceNumber3 = 6;
                    break;
                case "Side2":
                    DiceNumberTextScript3.diceNumber3 = 5;
                    break;
                case "Side3":
                    DiceNumberTextScript3.diceNumber3 = 4;
                    break;
                case "Side4":
                    DiceNumberTextScript3.diceNumber3 = 3;
                    break;
                case "Side5":
                    DiceNumberTextScript3.diceNumber3 = 2;
                    break;
                case "Side6":
                    DiceNumberTextScript3.diceNumber3 = 1;
                    break;
            }
        }
    }
}
