using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScript5 : MonoBehaviour {

	static Rigidbody rb5;
	public static Vector3 diceVelocity;
    public static Vector3 diceVelocity5;
    static Vector3 newPosition;

    // Use this for initialization
    void Start () {
		rb5 = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		diceVelocity5 = rb5.velocity;
        newPosition = new Vector3(-1.5f, 0.5f, -3.81f);

        if (Input.GetKeyDown (KeyCode.Space)) {
			DiceNumberTextScript1.diceNumber = 0;
			float dirX = Random.Range (0, 500);
			float dirY = Random.Range (0, 500);
			float dirZ = Random.Range (0, 500);
			transform.position = new Vector3 (0, 1, -9);
			transform.rotation = Quaternion.identity;
			rb5.AddForce (transform.up * 500);
			rb5.AddTorque (dirX, dirY, dirZ);
		}
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            gameObject.SetActive(!gameObject.activeSelf);
            rb5.detectCollisions = false;
            DiceNumberTextScript5.diceNumber5 = 0;
        }
        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * 2);
    }
}
