using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZoneScript4 : MonoBehaviour {
    Vector3 diceVelocity4;

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            GetComponent<Collider>().enabled = false;
            return;
        }
            diceVelocity4 = DiceScript4.diceVelocity;
    }

    private void OnTriggerEnter(Collider col4)
    {
        Debug.Log("Hehe");
        if (GetComponent<Collider>().enabled == false)
        {
            col4.enabled = false;
        }
    }

    void OnTriggerStay(Collider col4)
	{
        if (GetComponent<Collider>().enabled == false)
        {
            col4.enabled = false;
            //Debug.Log("collider is inactive, skip collision handling");
            //return;
        }
        else if (diceVelocity4.x == 0f && diceVelocity4.y == 0f && diceVelocity4.z == 0f)
        {
            switch (col4.gameObject.name)
            {
                case "Side1":
                    DiceNumberTextScript4.diceNumber4 = 6;
                    break;
                case "Side2":
                    DiceNumberTextScript4.diceNumber4 = 5;
                    break;
                case "Side3":
                    DiceNumberTextScript4.diceNumber4 = 4;
                    break;
                case "Side4":
                    DiceNumberTextScript4.diceNumber4 = 3;
                    break;
                case "Side5":
                    DiceNumberTextScript4.diceNumber4 = 2;
                    break;
                case "Side6":
                    DiceNumberTextScript4.diceNumber4 = 1;
                    break;
            }
        }
    }

    void OnTriggerExit(Collider col4)
    {
        if (GetComponent<Collider>().enabled == false)
        {
            col4.enabled = false;
        }
    }
}
