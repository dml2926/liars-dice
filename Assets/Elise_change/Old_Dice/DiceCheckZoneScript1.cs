using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZoneScript1 : MonoBehaviour {

	Vector3 diceVelocity;

    // Update is called once per frame
    void FixedUpdate () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            GetComponent<Collider>().enabled = false;
            return;
        }
        diceVelocity = DiceScript.diceVelocity;
    }

	void OnTriggerStay(Collider col)
	{
        if (GetComponent<Collider>().enabled == false)
        {
            col.enabled = false;
            //Debug.Log("collider is inactive, skip collision handling");
            //return;
        }
        else if (diceVelocity.x == 0f && diceVelocity.y == 0f && diceVelocity.z == 0f)
		{
			switch (col.gameObject.name) {
			case "Side1":
				DiceNumberTextScript1.diceNumber = 6;
				break;
			case "Side2":
				DiceNumberTextScript1.diceNumber = 5;
				break;
			case "Side3":
				DiceNumberTextScript1.diceNumber = 4;
				break;
			case "Side4":
				DiceNumberTextScript1.diceNumber = 3;
				break;
			case "Side5":
				DiceNumberTextScript1.diceNumber = 2;
				break;
			case "Side6":
				DiceNumberTextScript1.diceNumber = 1;
				break;
			}
		}
    }
}
