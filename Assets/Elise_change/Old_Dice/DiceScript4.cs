using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScript4 : MonoBehaviour {

	static Rigidbody rb4;
	public static Vector3 diceVelocity;
    public static Vector3 diceVelocity4;
    static Vector3 newPosition;

    // Use this for initialization
    void Start () {
		rb4 = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		diceVelocity4 = rb4.velocity;
        newPosition = new Vector3(-0.5f, 0.5f, -3.81f);

        if (Input.GetKeyDown (KeyCode.Space)) {
			DiceNumberTextScript1.diceNumber = 0;
			float dirX = Random.Range (0, 500);
			float dirY = Random.Range (0, 500);
			float dirZ = Random.Range (0, 500);
			transform.position = new Vector3 (0, 1, -9);
			transform.rotation = Quaternion.identity;
			rb4.AddForce (transform.up * 500);
			rb4.AddTorque (dirX, dirY, dirZ);
		}
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            gameObject.SetActive(!gameObject.activeSelf);
            rb4.detectCollisions = false;
            DiceNumberTextScript4.diceNumber4 = 0;
        }
        transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * 2);
    }
}
