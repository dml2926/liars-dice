﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class Register : MonoBehaviour
{
    public GameObject username;
    public GameObject email;
    public GameObject password;
    public GameObject conf_pass;
    private string Username;
    private string Email;
    private string Password;
    private string Conf_pass;
    private string form;
    private bool EmailValid = false;
    
    public void RegisterButton()
    {
        bool UN = false;
        bool EM = false;
        bool PW = false;
        bool CPW = false;

        if (Username != "Username...")
        {
            if (!System.IO.File.Exists(@Application.dataPath + "/Elise_change/Security/" + Username + ".txt"))
            {
                UN = true;
            } else
            {
                Debug.Log("Username Taken");
            }
        } else
        {
            Debug.Log("Username Field Empty");
        }
        if (Email != "Email...")
        {
            if (Email.Contains("@"))
            {
                if (Email.Contains("."))
                {
                     EM = true;
                } else
                {
                     Debug.Log("Email is Incorrect");
                }
                } else
                {
                    Debug.Log("Email is Incorrect");
                }
        } else
        {
            Debug.Log("Email Field is Empty");
        }
        if (Password != "password...")
        {
            if (Password.Length > 5)
            {
                PW = true;
            } else
            {
                Debug.Log("Password must be between 6-17 characters");
            }
        } else
        {
            Debug.Log("Password Field is Empty");
        }
        if (Conf_pass != "confirm password")
        {
            if (Conf_pass == Password)
            {
                CPW = true;
            } else
            {
                Debug.Log("Passwords do not match");
            }
        } else
        {
            Debug.Log("Confirm Password Field is Empty");
        }
        if (UN == true&&EM == true&&PW == true&&CPW == true)
        {
            bool Clear = true;
            int i = 1;
            foreach (char c in Password)
            {
                if (Clear)
                {
                    Password = "";
                    Clear = false;
                }
                i++;
                char Encrypted = (char)(c * i);
                Password += Encrypted.ToString();
            }
            form = (Username + "\n" + Email + "\n" + Password);
            System.IO.File.WriteAllText(@Application.dataPath + "/Elise_change/Security/" + Username + ".txt", form);
            username.GetComponent<InputField>().text = "";
            email.GetComponent<InputField>().text = "";
            password.GetComponent<InputField>().text = "";
            conf_pass.GetComponent<InputField>().text = "";
            Debug.Log("Registration Complete");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (Password != ""&&Email != ""&&Password != ""&&Conf_pass != "")
            {
                RegisterButton();
            }
        }

        Username = username.GetComponent<InputField>().text;
        Email = email.GetComponent<InputField>().text;
        Password = password.GetComponent<InputField>().text;
        Conf_pass = conf_pass.GetComponent<InputField>().text;
    }
}
