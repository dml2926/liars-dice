﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour
{
    public GameObject username;
    public GameObject password;
    private string Username;
    private string Password;
    private string[] Lines;
    private string DecryptedPass;

    public void LoginButton()
    {
        bool UN = false;
        bool PW = false;
        if (Username != "Username...")
        {
            if (System.IO.File.Exists(@Application.dataPath + "/Elise_change/Security/" + Username + ".txt"))
            {
                UN = true;
                Lines = System.IO.File.ReadAllLines(@Application.dataPath + "/Elise_change/Security/" + Username + ".txt");
            }
            else
            {
                Debug.Log("Username Invalid");
            }
        }
        else
        {
            //Debug.Log("Username Field Empty");
        }

        if (Password != "password...")
        {
            if (System.IO.File.Exists(@Application.dataPath + "/Elise_change/Security/" + Username + ".txt"))
            {
                int i = 1;
                foreach (char c in Lines[2])
                {
                    i++;
                    char Decrypted = (char)(c / i);
                    DecryptedPass += Decrypted.ToString();
                }
                if (Password == DecryptedPass)
                {
                    PW = true;
                } else
                {
                    Debug.Log("Password is Invalid");
                }
            } else
            {
                Debug.Log("Password is Invalid");
            }
        } else
        {
            //Debug.Log("Password Field is Empty");
        }
        if (UN == true&& PW == true)
        {
            username.GetComponent<InputField>().text = "";
            password.GetComponent<InputField>().text = "";
            Debug.Log("Login Successful");
            SceneManager.LoadScene(0);

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (Password != ""&&Password != "")
            {
                LoginButton();
            }
        }
        Username = username.GetComponent<InputField>().text;
        Password = password.GetComponent<InputField>().text;
    }
}
