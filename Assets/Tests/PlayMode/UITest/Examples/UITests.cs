﻿using UnityEngine;
using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;

public class UITests : UITest
{
    [UnityTest]
    public IEnumerator TitletoMainTest()
    {
        yield return LoadScene("TestableGameScene");

        //opens title scene
        yield return WaitFor(new ObjectAppeared<FirstScreen>());

        // Selects start and goes to main menu scene
        yield return Press("Start Button");

        yield return WaitFor(new ObjectAppeared<SecondScreen>());

        //looks for object only in main menu Scene
        if (GameObject.Find("Main Menu") == null)
        {
            Assert.Fail("Test Failed");
        } else
        {
            Assert.Pass("Test Passed");
        }
    }

    [UnityTest]
    public IEnumerator TitletoOfflineTest()
    {
        yield return LoadScene("TestableGameScene");

        //opens title scene
        yield return WaitFor(new ObjectAppeared<FirstScreen>());

        // Selects start and goes to main menu scene
        yield return Press("Start Button");

        yield return WaitFor(new ObjectAppeared<SecondScreen>());

        // Selects offline and goes to offline scene
        yield return Press("Offline Button");

        yield return WaitFor(new ObjectAppeared<ThirdScreen>());

        //looks for object only in Offline Scene
        if (GameObject.Find("Time est") == null)
        {
            Assert.Fail("Test Failed");
        }
        else
        {
            Assert.Pass("Test Passed");
        }
    }

    [UnityTest]
    public IEnumerator TitletoGameTest()
    {
        LogAssert.ignoreFailingMessages = true; //allows test to skip a small bug that isnt breaking the code but does fail the test

        yield return LoadScene("TestableGameScene");

        //opens title scene
        yield return WaitFor(new ObjectAppeared<FirstScreen>());

        // Selects start and goes to main menu scene
        yield return Press("Start Button");

        yield return WaitFor(new ObjectAppeared<SecondScreen>());
        LogAssert.ignoreFailingMessages = true;

        // Selects offline and goes to offline scene
        yield return Press("Offline Button");

        yield return WaitFor(new ObjectAppeared<ThirdScreen>());


        // Selects play and goes to game scene
        yield return Press("Play Button");

        yield return new WaitForSeconds(5f);

        //looks for object only in game Scene
        if (GameObject.Find("dice1") == null)
        {
            Assert.Fail("Test Failed");
        }
        else
        {
            Assert.Pass("Test Passed");
        }
        Debug.Break();
        Application.Quit();
    }
}

 