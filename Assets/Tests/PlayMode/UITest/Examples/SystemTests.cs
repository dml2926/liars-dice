﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine.TestTools;

public class SystemTests : UITest
{
    public GameObject dice1;
    static Rigidbody rb1;
    public static Vector3 diceVelocity;
    public static Vector3 diceVelocity1;
    static Vector3 newPosition;

    [UnityTest]
    public IEnumerator SystemTest()
    {
        //allows test to skip a small bug that isnt breaking the code but does fail the test
        LogAssert.ignoreFailingMessages = true;
        List<GameObject> characterList = new List<GameObject>();

        yield return LoadScene("TestableGameScene");

        //opens title scene
        yield return WaitFor(new ObjectAppeared<FirstScreen>());
        yield return new WaitForSeconds(2f);

        // Selects start and goes to main menu scene
        yield return Press("Start Button");
        yield return WaitFor(new ObjectAppeared<SecondScreen>());
        yield return new WaitForSeconds(2f);


        // Selects offline and goes to offline scene
        yield return Press("Offline Button");
        yield return WaitFor(new ObjectAppeared<ThirdScreen>());
        yield return new WaitForSeconds(2f);

        // Selects play and goes to game scene
        yield return Press("Play Button");
        yield return new WaitForSeconds(5f);

        GameObject dice6 = GameObject.Find("dice6");
        GameObject dice2 = GameObject.Find("dice2");
        GameObject dice3 = GameObject.Find("dice3");
        GameObject dice4 = GameObject.Find("dice4");
        GameObject dice5 = GameObject.Find("dice5");

        characterList.Add(dice2);
        characterList.Add(dice3);
        characterList.Add(dice4);
        characterList.Add(dice5);
        characterList.Add(dice6);


        bool pass = true;
        //First randomization
        foreach (GameObject objet in characterList)
        {
            rb1 = objet.GetComponent<Rigidbody>();

            diceVelocity1 = rb1.velocity;
            newPosition = new Vector3(2.5f, 0.5f, -3.81f);

            var rot1 = objet.transform.rotation;

            float dirX = Random.Range(0, 500);
            float dirY = Random.Range(0, 500);
            float dirZ = Random.Range(0, 500);
            objet.transform.position = new Vector3(0, 1, -9);
            objet.transform.rotation = Quaternion.identity;
            rb1.AddForce(objet.transform.up * 500);
            rb1.AddTorque(dirX, dirY, dirZ);

            yield return new WaitForSeconds(0.1f);

            var rot2 = objet.transform.rotation;

            if (rot1 == rot2)
            {
                pass = false;
            }
        }

        //Removes 2 dice
        yield return new WaitForSeconds(5f);
        dice6.GetComponent<Collider>().enabled = false;
        dice5.GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds(5f);

        //Second Rendomization
        foreach (GameObject objet in characterList)
        {
            rb1 = objet.GetComponent<Rigidbody>();

            diceVelocity1 = rb1.velocity;
            newPosition = new Vector3(2.5f, 0.5f, -3.81f);

            var rot1 = objet.transform.rotation;

            float dirX = Random.Range(0, 500);
            float dirY = Random.Range(0, 500);
            float dirZ = Random.Range(0, 500);
            objet.transform.position = new Vector3(0, 1, -9);
            objet.transform.rotation = Quaternion.identity;
            rb1.AddForce(objet.transform.up * 500);
            rb1.AddTorque(dirX, dirY, dirZ);

            yield return new WaitForSeconds(0.1f);

            var rot2 = objet.transform.rotation;

            if (rot1 == rot2)
            {
                pass = false;
            }
        }

        yield return new WaitForSeconds(5f);

        //checks dice randomization
        Assert.AreNotEqual(pass, false);

        //checks dice1 has been initialized and that its on the right scene
        if (GameObject.Find("dice1") == null)
        {
            Assert.Fail("Test Failed");
        }
        else
        {
            Assert.Pass("Test Passed");
        }

        //check that 2 dice have been removed
        Assert.AreEqual(dice6.GetComponent<Collider>().enabled, false);
        Assert.AreEqual(dice5.GetComponent<Collider>().enabled, false);

        foreach (GameObject objet in characterList)
        {
            GameObject.Destroy(objet);
        }


    }
}

