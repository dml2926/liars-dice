﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ThirdScreen : MonoBehaviour 
{

    [SerializeField] GameObject fourthScreenPrefab;
    [SerializeField] string responseText;

    public void OpenThirdScreen()
    {
        var s = Object.Instantiate(fourthScreenPrefab);
        s.name = fourthScreenPrefab.name;
        s.transform.SetParent(transform.parent, false);
        responseText = "Offline";
    }

    /*public void Close()
    {
        Destroy(gameObject);
    }	*/
}
