﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SecondScreen : MonoBehaviour 
{

    [SerializeField] GameObject thirdScreenPrefab;
    [SerializeField] string responseText;

    public void OpenThirdScreen()
    {
        var s = Object.Instantiate(thirdScreenPrefab);
        s.name = thirdScreenPrefab.name;
        s.transform.SetParent(transform.parent, false);
        responseText = "Main Menu";
    }

}
