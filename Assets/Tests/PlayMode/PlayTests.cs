﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests
{
    public class TestSuite
    {
        public GameObject dice1;
        static Rigidbody rb1;
        public static Vector3 diceVelocity;
        public static Vector3 diceVelocity1;
        static Vector3 newPosition;

        // A Test behaves as an ordinary method
        [Test]
        public void TestSimple()
        {
            bool isTest = true;
            Assert.AreEqual(isTest, true);
        }

        // A Test used to check that dice randomization is working
        [UnityTest]
        public IEnumerator TestDiceInit()
        {
            GameObject dice = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice.name = "dice1";
            bool exists = false;

            yield return new WaitForSeconds(0.1f);

            if (GameObject.Find("dice1") != null)
            {
                exists = true;
            }
            else
            {
                exists = false;
            }
            Assert.True(exists);

            Object.Destroy(dice);
        }

        [UnityTest]
        public IEnumerator TestHandInit()
        {
            List<GameObject> characterList = new List<GameObject>();

            GameObject dice1 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice1.name = "dice1";
            characterList.Add(dice1);

            GameObject dice2 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice2.name = "dice2";
            characterList.Add(dice2);

            GameObject dice3 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice3.name = "dice3";
            characterList.Add(dice3);

            GameObject dice4 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice4.name = "dice4";
            characterList.Add(dice4);

            GameObject dice5 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice5.name = "dice5";
            characterList.Add(dice5);

            yield return new WaitForSeconds(0.1f);


            Assert.AreEqual(characterList.Count, 5);

            foreach (GameObject objet in characterList)
            {
                GameObject.Destroy(objet);
            }
        }

        [UnityTest]
        public IEnumerator TestDiceDelete()
        {
            List<GameObject> characterList = new List<GameObject>();

            GameObject dice1 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice1.name = "dice1";
            characterList.Add(dice1);

            dice1.GetComponent<Collider>().enabled = false;

            yield return new WaitForSeconds(0.1f);


            Assert.AreNotEqual(dice1.GetComponent<Collider>().enabled, true);

            Object.Destroy(dice1);
        }

        [UnityTest]
        public IEnumerator TestDiceDeleteinHand()
        {
            List<GameObject> characterList = new List<GameObject>();

            GameObject dice1 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice1.name = "dice1";
            characterList.Add(dice1);

            GameObject dice2 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice2.name = "dice2";
            characterList.Add(dice2);

            GameObject dice3 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice3.name = "dice3";
            characterList.Add(dice3);

            GameObject dice4 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice4.name = "dice4";
            characterList.Add(dice4);

            GameObject dice5 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice5.name = "dice5";
            characterList.Add(dice5);

            dice3.GetComponent<Collider>().enabled = false;

            yield return new WaitForSeconds(0.1f);

            bool pass = true;

            foreach (GameObject objet in characterList)
            {
                if (objet.GetComponent<Collider>().enabled == false)
                {
                    pass = false;
                }
            }


            Assert.AreNotEqual(pass, true);

            foreach (GameObject objet in characterList)
            {
                GameObject.Destroy(objet);
            }
        }

        // A Test used to check that dice randomization is working
        [UnityTest]
        public IEnumerator TestDiceRandomization()
        {
            GameObject dice = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;

            rb1 = dice.GetComponent<Rigidbody>();

            diceVelocity1 = rb1.velocity;
            newPosition = new Vector3(2.5f, 0.5f, -3.81f);

            var rot1 = dice.transform.rotation;

            float dirX = Random.Range(0, 500);
            float dirY = Random.Range(0, 500);
            float dirZ = Random.Range(0, 500);
            dice.transform.position = new Vector3(0, 1, -9);
            dice.transform.rotation = Quaternion.identity;
            rb1.AddForce(dice.transform.up * 500);
            rb1.AddTorque(dirX, dirY, dirZ);

            yield return new WaitForSeconds(0.1f);

            var rot2 = dice.transform.rotation;


            Assert.AreNotEqual(rot1, rot2);

            GameObject.Destroy(dice);

        }

        // A Test used to check that dice randomization is working
        [UnityTest]
        public IEnumerator TestHandRandomization()
        {
            List<GameObject> characterList = new List<GameObject>();

            GameObject dice1 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice1.name = "dice1";
            characterList.Add(dice1);

            GameObject dice2 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice2.name = "dice2";
            characterList.Add(dice2);

            GameObject dice3 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice3.name = "dice3";
            characterList.Add(dice3);

            GameObject dice4 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice4.name = "dice4";
            characterList.Add(dice4);

            GameObject dice5 = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;
            dice5.name = "dice5";
            characterList.Add(dice5);

            bool pass = true;

            foreach (GameObject objet in characterList)
            {
                rb1 = objet.GetComponent<Rigidbody>();

                diceVelocity1 = rb1.velocity;
                newPosition = new Vector3(2.5f, 0.5f, -3.81f);

                var rot1 = objet.transform.rotation;

                float dirX = Random.Range(0, 500);
                float dirY = Random.Range(0, 500);
                float dirZ = Random.Range(0, 500);
                objet.transform.position = new Vector3(0, 1, -9);
                objet.transform.rotation = Quaternion.identity;
                rb1.AddForce(objet.transform.up * 500);
                rb1.AddTorque(dirX, dirY, dirZ);

                yield return new WaitForSeconds(0.1f);

                var rot2 = objet.transform.rotation;

                if (rot1 == rot2)
                {
                    pass = false;
                }
            }


            Assert.AreNotEqual(pass, false);

            foreach (GameObject objet in characterList)
            {
                GameObject.Destroy(objet);
            }

        }

        // unable to use colliders so had to create a work around
        [UnityTest]
        public IEnumerator TestDiceRead()
        {
            GameObject dice = MonoBehaviour.Instantiate(Resources.Load("dice"), new Vector3(2.5f, 0.5f, -3.81f), Quaternion.identity) as GameObject;

            rb1 = dice.GetComponent<Rigidbody>();

            diceVelocity1 = rb1.velocity;
            newPosition = new Vector3(2.5f, 0.5f, -3.81f);

            float dirX = Random.Range(0, 360);
            float dirY = Random.Range(0, 360);
            float dirZ = Random.Range(0, 360);
            dice.transform.position = new Vector3(0, 1, -9);
            dice.transform.rotation = Quaternion.identity;
            rb1.AddForce(dice.transform.up * 500);
            rb1.AddTorque(dirX, dirY, dirZ);

            yield return new WaitForSeconds(0.1f);
            int side = 0;

            switch (dirX)
            {
                case float n when (n >= 45):
                    if (dirZ >= 0 && dirZ <= 45)
                    {
                        side = 6;
                    }
                    if (dirZ > 45 && dirZ <= 135)
                    {
                        side = 2;
                    }
                    if (dirZ > 135 && dirZ <= 225)
                    {
                        side = 5;
                    }
                    if (dirZ > 225)
                    {
                        side = 6;
                    }
                    break;
                case float n when (n >= 135):
                    if (dirZ >= 0 && dirZ <= 45)
                    {
                        side = 1;
                    }
                    if (dirZ > 45 && dirZ <= 135)
                    {
                        side = 5;
                    }
                    if (dirZ > 135 && dirZ <= 225)
                    {
                        side = 6;
                    }
                    if (dirZ > 225)
                    {
                        side = 2;
                    }
                    break;
                case float n when (n >= 225):
                    if (dirZ >= 0 && dirZ <= 45)
                    {
                        side = 4;
                    }
                    if (dirZ > 45 && dirZ <= 135)
                    {
                        side = 3;
                    }
                    if (dirZ > 135 && dirZ <= 225)
                    {
                        side = 1;
                    }
                    if (dirZ > 225)
                    {
                        side = 4;
                    }
                    break;
                case float n when (n >= 315):
                    if (dirZ >= 0 && dirZ <= 45)
                    {
                        side = 4;
                    }
                    if (dirZ > 45 && dirZ <= 135)
                    {
                        side = 3;
                    }
                    if (dirZ > 135 && dirZ <= 225)
                    {
                        side = 1;
                    }
                    if (dirZ > 225)
                    {
                        side = 4;
                    }
                    break;
                default:
                    side = 0;
                    break;
            }
            bool pass = false;
            if (side >= 0 && side <= 6){
                pass = true;
            }

            Assert.AreEqual(pass, true);
            GameObject.Destroy(dice);

        }

    }
}
