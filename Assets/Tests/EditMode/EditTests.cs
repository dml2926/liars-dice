﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests
{
    public class EditTests
    {
        public GameObject dice1;
        static Rigidbody rb1;
        public static Vector3 diceVelocity;
        public static Vector3 diceVelocity1;
        static Vector3 newPosition;

        // testing proper initalization
        [Test]
        public void InitText()
        {
            string StartingText = "";
            StartingText = GameObject.FindGameObjectWithTag("Text1").GetComponent<Text>().text;

            Assert.AreEqual(StartingText, "0");
        }

        [Test]
        public void InitDice()
        {
            GameObject dice = GameObject.FindGameObjectWithTag("Dice1");

            Assert.IsNotNull(dice);
        }

        [Test]
        public void InitCheckZone()
        {
            GameObject Checkzone = GameObject.FindGameObjectWithTag("CheckZone1");

            Assert.IsNotNull(Checkzone);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TestWithEnumerator()
        {
            yield return null;
        }
    }
}
