﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    public bool inGame;

    public void SettingsBtn()
    {
        SceneManager.LoadScene(2);
    }
    public void BackBtn()
    {
        if (inGame == true) // if in game, go back to game
        {
            SceneManager.LoadScene(3);
        }
        else if (inGame == false) // else go back to main screen
        {
            SceneManager.LoadScene(0);
        }
        
    }
    public void OfflineBtn()
    {
        SceneManager.LoadScene(1);
    }
    public void GameBtn() // play button
    {
        inGame = true;
        SceneManager.LoadScene(3);
    }
    public void ExitBtn()
    {
        inGame = false;
        SceneManager.LoadScene(2);
    }
    public void StartBtn()
    {
        SceneManager.LoadScene(0);
    }
}
